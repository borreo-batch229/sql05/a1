[1]
SELECT customerName FROM customers WHERE country ="Philippines";

[2]
SELECT customers.contactLastName, customers.contactFirstName 
FROM customers WHERE customerName = "La Rochelle Gifts";

[3]
SELECT products.productName, products.MSRP FROM products WHERE productName = "The Titanic";

[4]
SELECT employees.firstName, employees.lastName FROM employees where email="jfirrelli@classicmodelcars.com";

[5]
SELECT customers.customerName FROM customers WHERE state IS NULL;

[6]
SELECT employees.firstName, employees.lastName, employees.email FROM employees WHERE lastName="Patterson" AND firstName="Steve";


[7]
SELECT customers.customerName, customers.country, customers.creditLimit FROM customers WHERE country!="USA" AND creditLimit>3000;


[8]
SELECT customers.customerNumber FROM customers JOIN orders ON customers.customerNumber = orders.customerNumber AND comments LIKE "%DHL%";

[9]
SELECT * FROM productlines WHERE textDescription LIKE "%state of the art%";

[10]
SELECT DISTINCT country FROM customers;

[11]
SELECT DISTINCT status FROM orders;


[12]
SELECT customers.customerName, customers.country FROM customers WHERE country IN("USA", "France", "Canada")

[13]
SELECT employees.firstName, employees.lastName, offices.city FROM employees JOIN offices ON employees.officeCode = offices.officeCode AND offices.city = "Tokyo";

[14]
SELECT customers.customerName FROM customers JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber AND firstName="Leslie" AND lastName="Thompson";

[15]
SELECT products.productName, customers.customerName FROM products 
JOIN orderdetails ON products.productCode = orderdetails.productCode
JOIN orders ON orderdetails.orderNumber = orders.orderNumber
JOIN customers ON orders.customerNumber = customers.customerNumber AND customerName = "Baane Mini Imports";


[16]
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country 
FROM employees 
JOIN offices ON employees.officeCode = offices.officeCode
JOIN customers ON offices.country = customers.country;


[17]
SELECT products.productName, products.quantityInStock FROM products WHERE productLine="planes" AND quantityInStock<1000;


[18]
SELECT customers.customerName FROM customers WHERE phone LIKE "%+81%";

